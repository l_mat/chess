#include "figures.h"

bool position_t::operator<(const position_t& other) const {
  return (first + 1000 * second) < (other.first + 1000 * other.second);
}

bool position_t::operator==(const position_t& other) const {
  return first == other.first && second == other.second;
}

const position_t figure::where() {
  return position;
}

std::ostream& operator<< (std::ostream& stream, figure& fig) {
  stream << fig.what();
  return stream;
}

std::ostream& operator<< (std::ostream& stream, position_t& pos) {
  stream << pos.first<<","<<pos.second;
  return stream;
}
