#include <string>
#include <sstream>
#include <iostream>
#include <vector>

#include "board.h"

bool board::operator+= (figure *f) {
  position_t pos = f->where();
  if(!contains(pos)) {
    // OOR
    return false;
  }
  if(captured(pos)) {
    // CAPTURED
    return false;
  }
  // check if could capture any figure
  for(auto it : figures) {
    position_t p = it.first;
    if(f->captures(p.first,p.second)) {
      // WOULD CAPTURE
      return false;
    }
  }
  // will not threaten any piece on the board
  figures[f->where()] = f;
  for(size_t i = 0; i < width; ++i)
    for(size_t j = 0; j < height; ++j)
      if(f->captures(i,j))
        countCaptured[i][j]++;
  return true;
}

/* check if the square at pos is empty */
bool board::square_empty(position_t pos) {
  return !(figures.count(pos));
}

/* check if the square at pos is captured by any figure */
bool board::captured(position_t pos) {
  return contains(pos) && countCaptured[pos.first][pos.second] > 0;
}

/*
 *  get the symbol of the figure at the given position
 *  or EMPTY_SQUARE
 */
char board::figureAt(position_t pos) {
 if(figures.count(pos))
   return figures[pos]->what();
  else return EMPTY_SQUARE;
}

bool board::contains(position_t pos) {
  bool result = pos.first >= 0 && pos.second >= 0 && pos.first < width && pos.second < height;
  return result;
}

void board::clear() {
  for(size_t i = 0; i < width; ++i)
    for(size_t j = 0; j < height; ++j)
      countCaptured[i][j] = 0;
  figures.clear();
}

void board::remove(position_t pos) {
  if(figures.count(pos) > 0) {
    figure *f = figures[pos];

    /* decrement entries in captures array */
    for(size_t i = 0; i < width; ++i)
      for(size_t j = 0; j < height; ++j)
        if(f->captures(i,j))
          countCaptured[i][j]--;
    delete figures[pos];
    figures.erase(figures.find(pos));
  }
}

figure* board::makeFigure(char symbol,position_t position) {
  switch(symbol) {
    case 'r': return new rook(position);
    case 'n': return new knight(position);
    case 'b': return new bishop(position);
    case 'q': return new queen(position);
    case 'k': return new king(position);
    default: return nullptr;
  }
}

board::es_iterator& board::es_iterator::operator++() {
  int end_index = b->width * b->height;
  if(index >= end_index) {
    // OOR
  } else
  while(++index != end_index) {
    x = index % b->width;
    y = index / b->width;

    position_t pos(x,y);
    if(!b->captured(pos) && b->square_empty(pos))
      break;
  }
  return *this;
}

bool board::es_iterator::operator!= (const board::es_iterator& other) const {
  return other.index != index;
}

board::es_iterator board::es_begin() {
  es_iterator result(this);
  result.index = -1;
  ++result;
  return result;
}

board::es_iterator board::es_end() {
  es_iterator result(this);
  result.index = width * height;
  return result;
}

position_t board::es_iterator::operator* () {
  if(index >= b->width * b->height)
    std::cout << "OUT OF RANGE!" << std::endl; // OOR
  return position_t(x,y);
}

/*
 *  board::solve(symbols_begin,symbols_end) - if possible, put a set of figures represented
 *  by a vector of symbols
 *
 *  the method is implemented as recursion with returns and uses classes iterator listing
 *  only empty and not threatened squares.
 */

bool board::solve(std::vector<char>::iterator symbols,std::vector<char>::iterator end) {
  if(symbols == end) // all figures have been placed on the chessboard
    return true;

  for(es_iterator f = es_begin(); f != es_end(); ++f) {
    figure *fig = makeFigure(*symbols, *f);
    if((*this) += fig) {
      if(solve(symbols+1,end))
        return true;
      else
       this->remove(*f); // TODO could just use an iterator
    } else
      delete fig;
  }
  return false;
}

/*
 *  output board to a file
 *
 *  .........
 *  ....Q....
 *  ..R...... etc.
 */
std::ofstream& operator<< (std::ofstream& stream, board & b) {
  for(int i = b.height-1; i >= 0; i--) {
    for(int j = 0; j < b.width; j++) {
      position_t pos(j,i);
      if(b.figureAt(pos) == board::EMPTY_SQUARE )
       stream << ".";
      else
        stream << b.figureAt(pos);
    }
    if(i!=0) stream << std::endl;
  }
  return stream;
}
