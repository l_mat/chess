#ifndef UI_H_INCLUDED
#define UI_H_INCLUDED

bool readOptions(std::unordered_map<std::string, int>& options, std::string filename);
bool parse_position(std::string str,position_t& pos);
void printHelp();
void autoTests();

#endif // UI_H_INCLUDED
