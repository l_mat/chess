#ifndef FIGURES_H_INCLUDED
#define FIGURES_H_INCLUDED
#include <tuple>
#include <ostream>
#include <cstdlib>

// type for figure position or chessboard square address
struct _position_t {
  size_t first;
  size_t second;
  _position_t(size_t f, size_t s) : first(f), second(s) {};
  bool operator<(const _position_t& other) const;
  bool operator== (const _position_t& other) const;
  friend std::ostream& operator<< (std::ostream& stream, _position_t& fig);
};

using position_t = _position_t;
using figure_char = char;

/*
 *  interface class figure - a figure on a chessboard.
 *
 *  contains figure's position as it is meant to be an element of a sparse array
 *  representing layout of certain number of figures of each kind
 *  and calculate which squares are endangered by the figure.
 */
class figure {
private:
  figure(const figure &);
protected:
  position_t position;
public:
  figure() : position(position_t(0,0)) {};
  figure(const position_t pos) : position(pos) {};
  const position_t where(); // get the figure's position
  virtual figure_char what() = 0;
  virtual bool captures(size_t x, size_t y) = 0;
  friend std::ostream& operator<< (std::ostream& stream, figure& fig);
};

/*
 *  each figure class implements functions declared in figure class as pure virtual:
 *  - what() - returns a character representing the kind of figure
 *  - captures(x,y) - tells if the figure (possibly) threatens certain square
 */

class rook : public figure {
public:
  rook(const position_t pos) : figure(pos) { };
  virtual figure_char what() { return 'R'; }
  bool captures(size_t x, size_t y) {
    return x == position.first || y == position.second;
  }
};

class knight : public figure {
public:
  knight(const position_t pos) : figure(pos) { };
  virtual figure_char what() { return 'N';  }
  bool captures(size_t x, size_t y) {
    int dx = position.first - x,
        dy = position.second - y;
    return (abs(dx) == 2 && abs(dy) == 1) || (abs(dx) == 1 && abs(dy) == 2);
  }
};

class bishop : public figure {
public:
  bishop(const position_t pos) : figure(pos) { };
  virtual figure_char what() { return 'B';  }
    bool captures(size_t x, size_t y) {
    return (position.first + position.second == x + y) || (position.second - position.first == y - x);
  }
};

class queen : public figure {
public:
  queen(const position_t pos) : figure(pos) {};
  virtual figure_char what() { return 'Q'; }
  bool captures(size_t x, size_t y) {
    return (x == position.first || y == position.second) || // row/col
           (position.first + position.second == x + y) || // diag
           (position.second - position.first == y - x);
  }
};

class king : public figure {
public:
  king(const position_t pos) : figure(pos) {};
  virtual figure_char what() { return 'K'; }
  bool captures(size_t x, size_t y) {
    return abs(position.first - x) <= 1 && abs(position.second - y) <= 1;
  }
};

#endif // FIGURES_H_INCLUDED
