#ifndef DRAW_H_INCLUDED
#define DRAW_H_INCLUDED

#include "board.h"
/*
 *  print the board on an ansi terminal
 *
 *  with numeric row indexes
 *  and alphabetic column indexes
 */
std::ostream& operator<< (std::ostream& stream, board & b);

#endif // DRAW_H_INCLUDED
